package com.tintash.sampleproject.repository.news.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewsResponseModel(
    @SerializedName("articles") val articles: ArrayList<NewsModel>
) : Parcelable