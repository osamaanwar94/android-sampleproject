package com.tintash.sampleproject.repository.news

import com.tintash.sampleproject.repository.news.models.NewsResponseModel
import com.tintash.sampleproject.shared.network.ApiConstants
import retrofit2.Response
import retrofit2.http.GET

interface NewsService {

    @GET(ApiConstants.HEADLINES)
    suspend fun fetchHeadlines(): Response<NewsResponseModel>

}