package com.tintash.sampleproject.repository.news

import com.tintash.sampleproject.shared.network.makeApiCall
import javax.inject.Inject

class NewsRepository @Inject constructor(
    private val service: NewsService
) {

    suspend fun fetchNews() = makeApiCall {
        service.fetchHeadlines()
    }
}



