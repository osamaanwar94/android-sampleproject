package com.tintash.sampleproject.shared.network

import android.util.Log
import com.tintash.retrofitcoroutinewrapper.ErrorConsumer
import com.tintash.retrofitcoroutinewrapper.ErrorParser
import com.tintash.retrofitcoroutinewrapper.safeApiCall
import org.json.JSONObject
import retrofit2.Response

inline fun <reified T> makeApiCall(call: () -> Response<T>) = safeApiCall(call, ErrorParserImpl, ErrorConsumerImpl)

object ErrorParserImpl : ErrorParser<JSONObject> {
    override fun parseErrors(response: Response<*>): List<JSONObject>? {
        return null
    }
}

object ErrorConsumerImpl : ErrorConsumer {
    override fun consumeError(response: Response<*>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun consumeException(e: Exception): Boolean {
        Log.v("findMe", "consuming ErrorConsumerImpl")

        return true
    }
}