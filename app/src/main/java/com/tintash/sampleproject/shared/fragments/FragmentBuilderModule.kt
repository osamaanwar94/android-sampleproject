package com.tintash.sampleproject.shared.fragments

import dagger.Module

/**
 * Module specifying all fragments that must be injected from the Application level component
 *
 * The @ContributesAndroidInjector is used to associate a fragment with the Application level component
 * <p>
 * In case a fragment has its own sub-component it should be initialized in the following way
<code>
@Binds
@IntoMap
@FragmentKey(MyFragment.class)
abstract AndroidInjector.Factory<? extends Fragment> bindMyFragment(MyFragmentComponent.Builder builder);
</code>
 */
@Suppress("unused")
@Module
abstract class FragmentBuilderModule