package com.tintash.sampleproject.shared.interfaces

import android.content.Context
import android.os.Bundle
import androidx.annotation.LayoutRes

interface LayoutSetup {
    fun restoreBundleState(savedInstanceState: Bundle?)

    @LayoutRes
    fun getLayoutId(): Int

    fun setup(context: Context)
}