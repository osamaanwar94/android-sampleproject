package com.tintash.sampleproject.shared.network

import com.tintash.sampleproject.application.AppScope
import com.tintash.sampleproject.repository.news.NewsService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Module specifying the dependencies of Retrofit services used throughout the app
 *
 * Services must be annotated with the Application Level scope as there should only be one instance of a service
 */
@Module(
    includes = [
        NetworkModule::class
    ]
)
class ServicesModule {

    @Provides
    @AppScope
    fun providesAnnouncementService(retrofit: Retrofit): NewsService {
        return retrofit.create(NewsService::class.java)
    }

}
