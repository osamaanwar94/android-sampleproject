package com.tintash.sampleproject.shared.activities

import com.tintash.sampleproject.login.views.activities.LoginActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module specifying all activities that must be injected from the Application level component
 *
 * The @ContributesAndroidInjector is used to associate an activity with the Application level component
 * <p>
 * In case an activity has its own sub-component it should be initialized in the following way
<code>
@Binds
@IntoMap
@ActivityKey(MainActivity.class)
abstract AndroidInjector.Factory<? extends Activity> bindMainActivity(MainActivityComponent.Builder builder);
</code>
 */
@Suppress("unused")
@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector
    abstract fun bindLoginActivity(): LoginActivity


}