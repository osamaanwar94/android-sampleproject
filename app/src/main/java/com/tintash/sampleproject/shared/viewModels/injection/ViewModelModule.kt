package com.tintash.sampleproject.shared.viewModels.injection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tintash.sampleproject.application.AppScope
import com.tintash.sampleproject.login.viewModels.LoginViewModel
import com.tintash.sampleproject.shared.viewModels.EmptyViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Module specifying all viewModels that must be injected from the Application level component
 *
 * ViewModels must always be injected only from the Application level component as the viewLayer can be destroyed
 * and recreated multiple times during the lifecycle of a viewModel
 */
@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @AppScope
    abstract fun bindViewModelFactory(factory: InjectableViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(EmptyViewModel::class)
    abstract fun bindEmptyViewModel(viewModel: EmptyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel
}