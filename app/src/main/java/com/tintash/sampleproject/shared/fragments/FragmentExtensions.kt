package com.tintash.sampleproject.shared.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.tintash.sampleproject.shared.activities.getViewModelClass
import com.tintash.sampleproject.shared.viewModels.BaseViewModel


/**
 * An Extension function for cleaning the boiler plate code of the static newInstance method in each
 * fragment. All required fields must be added in the lambda method passed as the parameter
 *
 * <pre>
 * LoginFragment.newInstance(firstName: String, age: Int) = LoginFragment().withArgs {
 *     putString(BUNDLE_KEY_NAME, firstName)
 *     putInt(BUNDLE_KEY_AGE, age)
 * }
 * </pre>
 */
inline fun <T : Fragment> T.withArgs(argsBuilder: Bundle.() -> Unit): T {
    return this.apply {
        arguments = Bundle().apply(argsBuilder)
    }
}

fun <T : BaseViewModel> BaseFragment<T>.initViewModel(): T {
    val clazz = getViewModelClass<T>(javaClass)

    return ViewModelProviders.of(this, viewModelFactory).get(clazz).also {
        lifecycle.addObserver(it)
    }
}

fun <T : BaseViewModel> BaseBottomSheetFragment<T>.initViewModel(): T {
    val clazz = getViewModelClass<T>(javaClass)

    return ViewModelProviders.of(this, viewModelFactory).get(clazz).also {
        lifecycle.addObserver(it)
    }
}