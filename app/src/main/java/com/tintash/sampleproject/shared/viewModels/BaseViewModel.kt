package com.tintash.sampleproject.shared.viewModels

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import javax.inject.Inject

/**
 * BaseViewModel
 *
 * Each ViewModel in the app should ideally extend from {@code #BaseViewModel}
 * The BaseViewModel class is made LifeCycleObserver because it depends upon lifecycle callbacks to destroy any
 * ongoing rxJava subscriptions
 */
abstract class BaseViewModel(application: Application) : AndroidViewModel(application), LifecycleObserver {

    /**
     * RxJava CompositeDisposable
     *
     * Responsible for destroying any subscriptions to ongoing RxJava streams once the associated view layer
     * is destroyed
     *
     * Any ongoing subscription to a rxJava stream should be added in the compositeDisposable so that it gets
     * destroyed once the associated view layer is destroyed and does not cause a memory leak
     */


    protected fun getContext(): Context = getApplication()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    protected open fun onLifeCycleOwnerCreated() {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    protected open fun onLifeCycleOwnerResumed() {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    protected open fun onLifeCycleOwnerPaused() {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    protected open fun onLifeCycleOwnerDestroyed() {
    }
}

class EmptyViewModel @Inject constructor(application: Application) : BaseViewModel(application)