package com.tintash.sampleproject.shared.extensions

import android.content.Context
import android.widget.Toast
import androidx.annotation.DimenRes

fun Context.getCompatDimen(@DimenRes id: Int) = resources.getDimension(id)

fun Context.showLongToast(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
