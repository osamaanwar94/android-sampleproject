package com.tintash.sampleproject.shared.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.tintash.sampleproject.shared.interfaces.LayoutSetup
import com.tintash.sampleproject.shared.viewModels.BaseViewModel
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * BaseActivity
 *
 * Each activity in the project should ideally extend from {@code #BaseActivity}
 * Activities extending from BaseActivity should have a registry in either ActivityBuilderModule or
 * its own sub-component module. Otherwise it would cause a crash as in the onCreate method dependency injection
 * magic happens
 */
abstract class BaseActivity<T : BaseViewModel> : AppCompatActivity(),
    HasSupportFragmentInjector, LayoutSetup {

    /**
     * Custom ViewModelFactory for injecting the viewModels
     */
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    /**
     * Injector for fragments expecting to be injected from the Application Component
     */
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    /**
     * ViewModel Object
     *
     * This viewModel object
     */
    protected lateinit var viewModel: T

    /**
     *
     * @param savedInstanceState Bundle?
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        // Must be called before super.onCreate
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        restoreBundleState(savedInstanceState)

        setContentView(getLayoutId())

        viewModel = initViewModel()
        setup(this)
    }

    /**
     *
     * @param savedInstanceState Bundle?
     */
    override fun restoreBundleState(savedInstanceState: Bundle?) {

    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector
}