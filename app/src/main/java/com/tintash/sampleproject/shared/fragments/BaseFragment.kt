package com.tintash.sampleproject.shared.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.tintash.sampleproject.shared.interfaces.LayoutSetup
import com.tintash.sampleproject.shared.viewModels.BaseViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * BaseFragment
 *
 * Each fragment in the project should ideally extend from {@code #BaseFragment}
 * Fragments extending from BaseFragment should have a registry in either FragmentBuilderModule or
 * its own sub-component module. Otherwise it would cause a crash as in the onCreate method dependency injection
 * magic happens
 */
abstract class BaseFragment<T : BaseViewModel> : Fragment(), LayoutSetup {

    /**
     * Custom ViewModelFactory for injecting the viewModels
     */
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected lateinit var viewModel: T

    override fun onCreate(savedInstanceState: Bundle?) {
        // Must be called before super.onCreate
        AndroidSupportInjection.inject(this)

        super.onCreate(savedInstanceState)
        restoreBundleState(savedInstanceState)

        viewModel = initViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setup(view.context)
    }

    override fun restoreBundleState(savedInstanceState: Bundle?) {
    }
}