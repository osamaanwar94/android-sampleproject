package com.tintash.sampleproject.shared.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.tintash.sampleproject.BuildConfig
import com.tintash.sampleproject.application.AppScope
import com.tintash.sampleproject.shared.network.interceptors.HeaderInterceptor
import com.tintash.tokeninterceptor.TokenInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Module specifying all the dependencies used to create a retrofit object
 */
@Module
class NetworkModule {
    companion object {
        const val BASE_URL = BuildConfig.BASE_URL
        private const val TIMEOUT = 60L
    }

    @Provides
    @AppScope
    fun providesOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        headerInterceptor: HeaderInterceptor,
        tokenInterceptor: TokenInterceptor
    ): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .addInterceptor(headerInterceptor)
            .addInterceptor(tokenInterceptor)
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @AppScope
    fun providesHeaderInterceptor(): HeaderInterceptor {
        return HeaderInterceptor()
    }

    @Provides
    @AppScope
    fun providesTokenInterceptor(): TokenInterceptor {
        return TokenInterceptor(ApiConstants.HEADER_AUTHORIZATION, object : TokenInterceptor.TokenProvider {
            override fun getToken() = "TODO"
        })
    }

    @Provides
    @AppScope
    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return httpLoggingInterceptor
    }

    @Provides
    @AppScope
    fun providesGson(): Gson {
        return GsonBuilder()
            .setLenient()
            .create()
    }

    @Provides
    @AppScope
    fun providesGsonConverter(gson: Gson): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }

    /**
     * This method is not annotated with the Application level scope because we want a new retrofit object
     * each time a new service is created.
     *
     * This helps with the limit of simultaneous connections imposed by retrofit
     */
    @Provides
    fun providesRetrofit(okHttpClient: OkHttpClient, converterFactory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(converterFactory)
            .build()
    }
}