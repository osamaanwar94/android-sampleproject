package com.tintash.sampleproject.shared.activities

import androidx.lifecycle.ViewModelProviders
import com.tintash.sampleproject.shared.viewModels.BaseViewModel
import java.lang.reflect.ParameterizedType

@Suppress("UNCHECKED_CAST", "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
fun <T : BaseViewModel> getViewModelClass(aClass: Class<*>): Class<T> {
    val type = aClass.genericSuperclass

    return if (type is ParameterizedType) {
        type.actualTypeArguments[0] as Class<T>
    } else {
        getViewModelClass(aClass.superclass)
    }
}

fun <T : BaseViewModel> BaseActivity<T>.initViewModel(): T {
    return initViewModel(getViewModelClass(javaClass))
}

fun <T : BaseViewModel> BaseActivity<*>.initViewModel(clazz: Class<T>): T {
    return ViewModelProviders.of(this, viewModelFactory).get(clazz).also {
        lifecycle.addObserver(it)
    }
}