package com.tintash.sampleproject.shared.viewModels.injection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tintash.sampleproject.application.AppScope
import javax.inject.Inject
import javax.inject.Provider

/**
 * A Custom ViewModel Provider Factory for injecting viewModels using Dagger
 */
@AppScope
class InjectableViewModelFactory @Inject constructor(
    private val viewModelProviders: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
) : ViewModelProvider.Factory {

    /**
     * All ViewModels expecting to be injected from this factory must be defined in the ViewModelModule and have
     * a unique ViewModelKey associated with it, Otherwise this method will always throw an exception
     */
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val provider = viewModelProviders[modelClass]

        provider?.let {
            return it.get() as T
        } ?: run {
            throw IllegalStateException("No Provider could be found for the given key")
        }
    }
}