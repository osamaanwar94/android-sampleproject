package com.tintash.sampleproject.shared.network.interceptors

import android.os.Build
import com.tintash.sampleproject.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

/**
 * OkHttp Interceptor for adding headers in network requests
 * <p>
 * If a header is required to be added in all network requests than it should be handled in this class
 * <p>
 * In case a header is added to the network request conditionally then it should have its own interceptor like
 * {@code #TokenInterceptor}
 */
class HeaderInterceptor : Interceptor {
    companion object {
        private const val VERSION = 1

        private const val HEADER_CONTENT_TYPE = "Content-Type"
        private const val HEADER_ACCEPT = "ACCEPT"
        private const val USER_AGENT = "User-Agent"

        private const val HEADER_CONTENT_TYPE_VAL = "application/json"
        private const val HEADER_ACCEPT_VAL = "version=$VERSION"
    }

    /**
     * Intercepts the network call
     *
     * Adds the required headers to the network call in order to be accepted by the server
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBuilder = request.newBuilder()

        requestBuilder.addHeader(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_VAL)
        requestBuilder.addHeader(HEADER_ACCEPT, HEADER_ACCEPT_VAL)
        requestBuilder.addHeader(USER_AGENT, getUserAgent())

        return chain.proceed(requestBuilder.build())
    }

    /**
     * Returns the user-agent field value
     */
    private fun getUserAgent(): String {
        val versionName = BuildConfig.VERSION_NAME
        val applicationId = BuildConfig.APPLICATION_ID
        val versionCode = BuildConfig.VERSION_CODE
        val osVersion = Build.VERSION.RELEASE

        return "SampleApplication/$versionName ($applicationId; build:$versionCode; Android $osVersion)"
    }
}