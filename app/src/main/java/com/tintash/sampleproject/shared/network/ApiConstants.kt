package com.tintash.sampleproject.shared.network

object ApiConstants {
    private const val NEWS_API_KEY = "6be6b205658b459b8eb2936d6fe6b0b1"

    const val HEADER_AUTHORIZATION = "Authorization"

    const val HEADLINES = "/v2/top-headlines?sources=google-news&apiKey=$NEWS_API_KEY"
}