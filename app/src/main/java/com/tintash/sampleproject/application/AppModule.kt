package com.tintash.sampleproject.application

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * AppModule
 *
 * Application level dependencies that can not grouped with similar dependencies e.g. ApplicationContext
 * belong in this class
 */
@Module
class AppModule {

    @Provides
    fun providesContext(application: Application): Context {
        return application.applicationContext
    }
}