package com.tintash.sampleproject.application

import javax.inject.Scope

/**
 * Custom Dagger scope for application level dependencies
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope