package com.tintash.sampleproject.application

import android.app.Activity
import android.app.Application
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Custom Application class
 *
 * Responsible for providing Activity, Service and broadcast injectors
 */
class SampleApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    /**
     * Method to initialize dagger App component
     *
     * This method should be set to protected and overridden in the mockApplication class
     * in case a mock dagger graph is needed for test cases
     */
    private fun initDagger() {
        DaggerAppComponent
            .builder()
            .application(this)
            .build()
            .inject(this)
    }

    override fun activityInjector() = dispatchingAndroidInjector
}