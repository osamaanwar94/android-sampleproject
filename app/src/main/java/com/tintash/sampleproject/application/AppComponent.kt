package com.tintash.sampleproject.application

import android.app.Application
import com.tintash.sampleproject.shared.activities.ActivityBuilderModule
import com.tintash.sampleproject.shared.fragments.FragmentBuilderModule
import com.tintash.sampleproject.shared.network.ServicesModule
import com.tintash.sampleproject.shared.viewModels.injection.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

/**
 * AppComponent responsible for providing application level dependencies
 */
@AppScope
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ActivityBuilderModule::class,
        FragmentBuilderModule::class,
        ViewModelModule::class,
        ServicesModule::class
    ]
)
interface AppComponent : AndroidInjector<SampleApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}