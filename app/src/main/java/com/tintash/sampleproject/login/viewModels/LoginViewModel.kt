package com.tintash.sampleproject.login.viewModels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.tintash.retrofitcoroutinewrapper.ResponseWrapper
import com.tintash.retrofitcoroutinewrapper.createOnGoing
import com.tintash.retrofitcoroutinewrapper.createResponseModel
import com.tintash.sampleproject.repository.news.NewsRepository
import com.tintash.sampleproject.repository.news.models.NewsResponseModel
import com.tintash.sampleproject.shared.viewModels.BaseViewModel
import kotlinx.coroutines.launch
import org.json.JSONObject
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    application: Application,
    private val repo: NewsRepository
) : BaseViewModel(application) {

    val liveData = MutableLiveData<ResponseWrapper<NewsResponseModel, JSONObject>>()

    init {
        fetchNews()
    }

    private fun fetchNews() {
        liveData.postValue(ResponseWrapper.createOnGoing())

        viewModelScope.launch {
            liveData.postValue(repo.fetchNews().createResponseModel())
        }
    }
}