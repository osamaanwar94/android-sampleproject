package com.tintash.sampleproject.login.views.activities

import android.content.Context
import android.util.Log
import androidx.lifecycle.Observer
import com.tintash.sampleproject.R
import com.tintash.sampleproject.login.viewModels.LoginViewModel
import com.tintash.sampleproject.shared.activities.BaseActivity
import com.tintash.sampleproject.shared.viewModels.EmptyViewModel

class LoginActivity : BaseActivity<LoginViewModel>() {

    override fun getLayoutId() = R.layout.activity_login

    override fun setup(context: Context) {
        abc()
    }

    private fun abc() {
        viewModel.liveData.observe(this, Observer {
            Log.v("findMe", it.toString())
        })
    }
}
